__global__ void test(int** a) {
	*a = (int*)malloc(1);
}

int main() {
	int** a;
	cudaMalloc(&a, 16);
	test<<<1,1>>>(a);
	cudaDeviceSynchronize();
	cudaFree(a);
	cudaDeviceReset();
}
