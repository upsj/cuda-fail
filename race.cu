__global__ void test(int* a, int* b) {
	__shared__ int i;
	i = *b;
	i++;
	__syncthreads();
	*a = i;
}

int main() {
	int* a;
	cudaMalloc(&a, 16);
	test<<<1,32>>>(a, a);
	cudaDeviceSynchronize();
	cudaFree(a);
}
