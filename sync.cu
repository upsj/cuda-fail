__global__ void test(int* a, int* b) {
	*a = *b;
	if (threadIdx.x == 0) __syncthreads();
	*a += *b;
}

int main() {
	int* a;
	cudaMalloc(&a, 16);
	test<<<1,2>>>(a, a);
	cudaDeviceSynchronize();
	cudaFree(a);
}
